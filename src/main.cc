#include<iostream>
#include<cmath>

using namespace std;

int check_prime(int);

int main(int argc, char *argv[]) {
	// 引数が足りない場合「引数が足りない」とエラーを出力する
	if (argc < 2) {
		cerr << "Some arguements are missing!" << endl;
	} else {
		// 「-c」が一つ目の引数だった場合
		if (argv[1] == string("-c")) {
			int i, count = 0;
			for (i = 1; i <= atoi(argv[2]); i++) {
				// 返り値が0か1なので直接加算にする
				count += check_prime(i);
			}
			cout << "There are " << count << " prime numbers in 1 to "
				<< atoi(argv[2]) << endl;
		} else {
			// 素数だった場合
			if (check_prime(atoi(argv[1]))) {
				cout << atoi(argv[1]) << " is prime." << endl;
			// 素数ではない場合
			} else {
				cout << atoi(argv[1]) << " is not prime." << endl;
			}
		}
	}

	return 0;
}

int check_prime(int n) {
	if (n < 2) {
		return 0;
	} else if (n == 2) {
		return 1;
	} else if (n % 2 == 0) {
		return 0;
	} else {
		int i, flg = 1, n_sqrt = sqrt(n);
		// for (i = 3; i < n; i += 2) {
		for (i = 3; i <= n_sqrt; i += 2) {  // ←①
			if (n % i == 0) {
				flg = 0;
				break;
			}
		}
		return flg;
	}
}
