#!/bin/bash

if [ ! -d "builddir" ]; then
    CC=clang CXX=clang++ meson builddir
fi
ninja -C builddir
./builddir/prime $@