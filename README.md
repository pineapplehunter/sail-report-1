# 素数を求めるプログラム
このレポジトリには、レポートに添付してあるファイルがおいてある。

## 前提条件
私はLinux(Ubuntu)環境で実行しているため他の環境では動くかわからない。

### Ubuntuでのパッケージ
```bash
$ apt install meson ninja-build clang
```

## 実行
必要なパッケージがインストールされていれば次の方法で実行できる
```bash
$ chmod +x ./run.sh
$ ./run.sh 7
# ビルド関連の出力
7 is prime.
```
